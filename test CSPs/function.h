#include <iostream>
#include <iomanip>
#include <windows.h>
#include <cstdlib>
#include <ctime>
#include <time.h>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdio>
#include <io.h>
#include "graphics.h"
#pragma comment(lib, "graphics.lib")
#pragma warning(disable : 4996)
using namespace std;

void importData (string fileName, vector <vector<int>> &point, int &vertice, int &edge, int &degMax);
void printSolution(int color[], int V) ;
bool isSafe (vector <vector<int>> graph, int color[], int c, int v, int V);
bool graphColoringUtil(vector <vector<int>> graph, int &m, int color[], int &v,  int V); 
bool graphColoring(vector <vector<int>> graph, int color[], int &m, int key, int V);
void start();
void runningTime(double end);
void present();
void graphDrawing (vector <vector<int>> edge, int color[], int vertice);