#include "function.h"
void importData (string fileName, vector <vector<int>> &point, int &vertice, int &edge, int &degMax)
{
	vector <int> deg;
	
	string str = "instances/" + fileName;

	char * input = new char[str.size() + 1];;
	strcpy(input, str.c_str()); 
	
	while (access(input, 0) == -1)
    {
		cout << "File not found, input again: ";
        cin >> fileName;
		str = "instances/" + fileName;
		strcpy(input, str.c_str()); 
    }
   
	ifstream infile(input);
	if (infile) 
	{
		string line;
		while (getline(infile, line)) 
		{	
			istringstream iss(line);
			string data1, data2, data3, data4;
			int i = 1;
			while(iss)
			{
				string sub;
				iss >> sub;
				if (i == 1)
					data1 = sub;
				else if (i ==  2)
					data2 = sub;
				else if (i == 3)
					data3 = sub;
				else if (i == 4)
					data4 = sub;	
				i++;
			}
			if (data1 == "p" & data2 == "edge")
			{
				stringstream convert1(data3);
				convert1 >> vertice;
				stringstream convert2 (data4) ;
				convert2 >> edge;
				//cout << vertice << endl << edge << endl;
				point.resize(vertice+1);
				deg.resize(vertice+1);

				for (int i  = 0; i <= vertice; i++)
				{
					point[i].resize(vertice+1);
				}
				
			}
			else if (data1 == "e")
			{
				int e1, e2;
				stringstream convert1(data2);
				convert1 >> e1;
				stringstream convert2 (data3) ;
				convert2 >> e2;
				point[e1][e2] = 1;
				point[e2][e1] = 1;
				
			}
		}
		infile.close();
	}

	for (int i = 1; i <= vertice; i++)
	{
		
		for (int j = 1; j <=vertice; j++)
		{
			if (point[i][j] == 1)
				deg[i] +=1;
		}
	}

	int temp;
	for (int i = 1; i <= vertice; i++)
	{
		for (int j = i +1; j <= vertice; j++)
		{
			if (deg[i] <= deg[j])
			{
				temp = deg[i];
				deg[i] = deg[j];
				deg[j] = temp;
			}
		}

	}
	degMax = deg[1];
	cout << "Path: " << input << endl;
}