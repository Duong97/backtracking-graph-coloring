#include "function.h"

void runningTime(double end)
{
	double S;
	int M, H;
	if (end < 60)
	{
		S = end;
		cout << setprecision(2) << S << "s";
	}
	else if (end <= 60*60)
	{
		M = end/60;
		cout << M << "m ";
		S = end - M*60;
		cout << setprecision(2) << S << "s";
	}
	else
	{
		M = end/60;
		H = M/60;
		cout << H << "h ";
		M = M - H*60;
		cout << M << "m ";
		S = end - M*60;
		cout << setprecision(2) << S << "s";
	}
}

void present()
{
	time_t present = time(0);
	tm *gmtm = gmtime(&present);
	char* dt = ctime(&present);
	cout << dt << endl;
}