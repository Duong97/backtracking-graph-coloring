#include "function.h";

void printSolution(int color[], int V) 
{ 
	cout << endl << "Configuration: " << endl; 
	for (int i = 1; i <= V; i++)
		cout << "vertice " << i << ": " << color[i] << endl;
	cout << endl;
} 

bool isSafe (vector <vector<int>> graph, int color[], int c, int v, int V) 
{ 
	for (int i = 1; i <= V; i++) 
		if (graph[v][i] == 1 && c == color[i]) 
			return false; 
	return true; 
} 

bool graphColoringUtil(vector <vector<int>> graph, int &m, int color[], int &v,  int V) 
{ 
	if (v == V+1) 
        return true; 

	for (int c = 1; c <= m; c++) 
	{ 
		if (isSafe(graph, color, c, v, V)) 
		{ 
			color[v] = c; 
			v = v+1;
			if (graphColoringUtil (graph, m, color, v, V) == true) 
				return true;
		} 
	} 
	//If no color can be assigned to this vertex then return false
	return false;
} 

bool graphColoring(vector <vector<int>> graph, int color[], int &m, int key, int V) 
{ 
	
	int v = 1;
	cout << endl << "Processing... " << endl;

	if (key == 2)
	{
		while(graphColoringUtil(graph, m, color, v, V) == false)
		{
			cout << ".";
			m+=1;
			color[v] = m;
			v+=1;
		}
		printSolution(color, V); 
		return true; 
	}

	if (key == 1)
	{
		if (graphColoringUtil(graph, m, color, v, V) == false)
		{
			cout << "Cannot color graph" << endl;
			return false;
		}
		else
		{
			printSolution(color, V); 
			return true; 
		}
	}
} 