#include "function.h"

typedef struct point {
	int x, y;
	int color;
} point_t;

void graphDrawing (vector <vector<int>> edge, int color[], int vertice)
{
	vector <point_t> point;
	vector <int> colorFake;

	int v, h, w, check;
	v = vertice;
	if (v < 100) 
	{
		w = 500;
		h = 250;
	}
		
	else if (v < 500)
	{
		w = 1000;
		h = 500;
	}
	else
	{
		w = 2000;
		h = 1000;
	}

	initwindow(w, h);
	point.resize(v+1);
	colorFake.resize(v+1);
	for (int i = 1; i <= v; i++)
		colorFake[i] = color[i];
	
	
	for (int i = 1; i <= v; i++)
	{
		point[i].color = colorFake[i];
		check = 0;
		while (true)
		{
			point[i].x = rand () % (w -24 - 24 + 1) + 24;
			point[i].y = rand () % (h - 24 - 24 + 1) + 24;
			for (int j = i-1; j >= 1; j--)
				if((sqrt(pow(point[i].x - point[j].x,2) +  pow(point[i].y - point[j].y,2)) <= 12*2))
					check = 1;
			if (check == 0) 
				break;
			else 
				check = 0;
		}
	}

	for (int i = 1; i <= v; i++)
	{
		for (int j = 1; j <= v; j ++)
		{
			if (edge[i][j] == 1)
			{
				setcolor(12);
				line (point[i].x, point[i].y, point[j].x, point[j].y);
			}

		}

	}

	for (int i = 1; i <= v; i++)
	{
		setcolor(15);
		circle (point[i].x, point[i].y, 12);

		stringstream ss;
		ss << i;
		string str = ss.str();
		char * input = new char[str.size() + 1];;
		strcpy(input, str.c_str()); 
		setcolor(15);
		outtextxy(point[i].x-8, point[i].y-8, input);

		stringstream si;
		si << point[i].color;
		str = si.str();
		strcpy(input, str.c_str()); 
		setcolor(14);
		outtextxy(point[i].x-8, point[i].y-8+15, input);
	}

	getch();
	closegraph();   
}