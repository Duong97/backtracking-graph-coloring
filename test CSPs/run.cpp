#include "function.h"

void start() 
{ 
	vector <vector<int>> point;
	int vertice, edge, degMax;
	string fileName;
	int m;
	int key;
	
	cout << "File name: ";
	cin >> fileName;
	importData (fileName, point, vertice, edge, degMax);
	cout << "Max Degree: " << degMax << endl << "Vertices: " << vertice << endl << "edges: " << edge << endl;
	cout << "Press 1 to input number of colors manually, press 2 to run automatically: ";
	cin >> key;

	while (!(key == 1 | key == 2))
	{
		cout << "Select option again: ";
		cin >> key;
	}

	if (key == 2)
		m = 1;
	else
	{
		cout << "Input number of color: ";
		cin >> m;
	}

	int *color = new int[vertice+1]; 
	for (int i = 1; i <= vertice; i++) 
		color[i] = 0;

	clock_t start = clock();
	double end; 
	bool result;

	result = graphColoring (point, color, m, key, vertice);
	end = (double)(clock() - start)/CLOCKS_PER_SEC;

	if (key == 2) 
		cout << endl << "Chromatic number: " << m << endl;

	if (result == true)
	{
		cout << "Time: ";
		runningTime(end);

		cout << endl;

		present();

		cout << "Starting initialize graph..."<< endl;
		graphDrawing (point, color, vertice);
	}

	system("pause");
} 
