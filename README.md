This is an assignment of AI course in 2019. The graph will be coloured with at most m colours such that there are no two adjacent vertices which are colored with the same color. 

This figure represents the result of graph colouring. The number inside each circle is the index of vertice, while the index under circle is colour code for each vertice.

![IMAGE_DESCRIPTION](./Graphcoloring_demo.png)
